VERSION=`git describe --tags`
BUILD_TIME=`date +%FT%T%z`
LDFLAGS=-ldflags "-X main.Version=${V} -X main.BuildTime=${BUILD_TIME}"
SER=rhthymini

build-docker-img: clear
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build ${LDFLAGS} -o ./bin/$(SER) ./service/$(SER)/main.go
	docker build --build-arg SERVICE=$(SER) -t harbor.lko.hvac-cloud.org/ydt/$(SER):$(V) .
	docker push harbor.lko.hvac-cloud.org/ydt/$(SER):$(V)
	docker rmi $$(docker images |grep '$(SER)')

build-gcp-docker: clear
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build ${LDFLAGS} -o ./bin/$(SER) ./main.go
	docker build --build-arg SERVICE=$(SER) -t ydt/$(SER):$(V) .
	docker tag ydt/$(SER):$(V) asia.gcr.io/muulin-universal/$(SER):$(V)
	docker tag ydt/$(SER):$(V) asia.gcr.io/muulin-universal/$(SER):latest
	docker push asia.gcr.io/muulin-universal/$(SER):$(V)
	docker push asia.gcr.io/muulin-universal/$(SER):latest

run: build
	./bin/$(SER) -confpath ./config/etc/config-${ENV}/

build: clear
	go build ${LDFLAGS} -o ./bin/$(SER) ./main.go
	./bin/$(SER) -v

build-win: clear-win
	GOOS=windows GOARCH=386 go build -o ./build/exe/$(SER).exe ./main.go

clear-win:
	rm -rf ./build/exe/*

clear:
	rm -rf ./bin/$(SER)

clear-untag-image:
	docker rmi $(docker images --filter “dangling=true” -q --no-trunc)


# ## Push image

# gcloud auth configure-docker
# docker tag <image-name>:<tag> asia.gcr.io/muulin-universal/<image-name>:<tag>
# docker push asia.gcr.io/muulin-universal/<image-name>:<tag>

# ## get Kubernetes config
# gcloud container clusters get-credentials muulin-gcp-1 --zone=asia-east1

# ## test 
# kubectl get nodes