package usr

import (
	"bytes"
	"encoding/json"
	"fmt"
	"reflect"
	"strings"
)

type Parser interface {
	AutoUploadRes() *AutoUploadContent
	IsAutoUploadRes() bool
	GetMAC() string
	GetContent() string
}

var (
	heartpkt    = []byte("Heartpkt")
	lenHeartpkt = len(heartpkt)
)

func IsHeartpkt(data []byte) bool {
	if len(data) != lenHeartpkt {
		return false
	}
	for i := 0; i < lenHeartpkt; i++ {
		if data[i] != heartpkt[i] {
			return false
		}
	}
	return true
}

func NewParser(data []byte) (Parser, error) {
	res := &response{}
	data = replaceNewLine(data)
	err := json.NewDecoder(bytes.NewReader(replaceNewLine(data))).Decode(res)
	if err != nil {
		return nil, err
	}
	return &parserImp{response: res}, nil
}

func replaceNewLine(data []byte) []byte {
	return bytes.ReplaceAll(
		bytes.ReplaceAll(data, []byte("\r\n"), []byte("&")),
		[]byte("\n"), []byte("&"))
}

type parserImp struct {
	*response
}

type response struct {
	Msg     string `json:"msg"`
	Mac     string `json:"id"`
	Content string `json:"content"`
}

type AutoUploadContent struct {
	Mac     string `usr:"MAC"`
	Version string `usr:"VER"`
	// 查询 SYSINFO 信息
	// +SYSINFO:ops_operate,ops_net_type
	// ops_operate:运营商信息 ops_net_type:驻网模式
	SysInfo string `usr:"SYSINFO"`
	// 查询模块获取到的 WAN 口 IP(DHCP/STATIC)
	// mode,address,mask,gateway
	Wann string `usr:"WANN"`
	// 查询或设置 APN 信息
	// +APN:apn_name,user,pw,type
	APN string `usr:"APN"`
	// 查询时间段 4G 流量
	// <rx>,<tx>,<pro_time>,<at_time>
	// rx:距离上次查询至本次查询时间段接收字节数
	// tx:距离上次查询至本次查询时间段发送字节数
	// pro_time:上次使用本指令时间戳
	// at_time:本次使用本指令时间戳
	Traffic string `usr:"TRAFFIC"`
	// 查询时间段 wan 流量
	// <rx>,<tx>,<pro_time>,<at_time>
	// rx:距离上次查询至本次查询时间段接收字节数
	// tx:距离上次查询至本次查询时间段发送字节数
	// pro_time:上次使用本指令时间戳
	// at_time:本次使用本指令时间戳
	WiredTraffic string `usr:"WIREDTRAFFIC"`
	// 查询设备当前信号强度信息
	CSQ string `usr:"CSQ"`
	// 查询系统运行时间
	Uptime string `usr:"UPTIME"`
	// 查询默认路由使用网卡情况
	// STATUS( 网络类型):wired、4G、sta
	NetStatus string `usr:"NETSTATUS"`
	// 4G网络注册状态
	// 1代表注册成功，其他值未注册网络
	Cgreg string `usr:"CGREG"`
}

func (c *AutoUploadContent) SetByTag(fieldName, val string) error {
	v := reflect.ValueOf(c).Elem()
	if !v.CanAddr() {
		return fmt.Errorf("cannot assign to the item passed, item must be a pointer in order to assign")
	}
	findUsrName := func(t reflect.StructTag) (string, error) {
		if jt, ok := t.Lookup("usr"); ok {
			return strings.Split(jt, ",")[0], nil
		}
		return "", fmt.Errorf("tag provided does not define a json tag: %s", fieldName)
	}
	fieldNames := map[string]int{}
	for i := 0; i < v.NumField(); i++ {
		typeField := v.Type().Field(i)
		tag := typeField.Tag
		jname, _ := findUsrName(tag)
		fieldNames[jname] = i
	}
	fieldNum, ok := fieldNames[fieldName]
	if !ok {
		return fmt.Errorf("field %s does not exist within the provided item", fieldName)
	}
	fieldVal := v.Field(fieldNum)
	fieldVal.Set(reflect.ValueOf(val))
	return nil
}

func (p *parserImp) IsAutoUploadRes() bool {
	return p.Msg == "remoteAutoUploadRes"
}

func (p *parserImp) GetMAC() string {
	return p.Mac
}

func (p *parserImp) GetContent() string {
	return p.Content
}

func (p *parserImp) AutoUploadRes() *AutoUploadContent {
	if p.Msg != "remoteAutoUploadRes" {
		return nil
	}
	cp := newContentParser(p.Content)
	var k, v string
	content := &AutoUploadContent{}
	var err error
	for {
		k, v = cp.nextTerm()
		if k == "" {
			break
		}
		err = content.SetByTag(k, v)
		if err != nil {
			fmt.Println(k, v)
		}
	}
	return content
}

func newContentParser(c string) *contentParser {
	return &contentParser{
		content: c,
		size:    len(c),
	}
}

type contentParser struct {
	content string
	size    int
	start   int
}

func (cp *contentParser) nextTerm() (key string, val string) {
	score := 0
	var scan byte
	var term []byte
	colonCount := 0
	for ; cp.start < cp.size; cp.start++ {
		scan = cp.content[cp.start]
		if score == 3 && scan == '&' {
			val = string(term)
			term = term[len(term):]
			score = 0
			break
		}
		if score == 3 && scan == ':' && colonCount < 1 {
			key = string(term)
			colonCount++
			term = term[len(term):]
			continue
		}
		if score == 3 {
			term = append(term, scan)
			continue
		}
		if scan != '&' && scan != '+' {
			score = 0
			continue
		}
		score++
	}
	return
}
