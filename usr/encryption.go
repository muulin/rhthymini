package usr

import "bytes"

var (
	key = [16]byte{
		0x56, 0x38, 0xF6, 0xBC, 0x52, 0x15, 0x51, 0x25, 0x68, 0x74, 0x46, 0x13, 0x7F, 0xD6, 0x5A, 0xDF,
	}
)

type Encryption interface {
	EncryptBlock(str []byte) []byte
	DecryptBlock(str []byte) []byte
}

func NewEncryption() Encryption {
	return &usrImpl{}
}

type usrImpl struct {
	decryptKey      [16]byte
	inter, cfc, cfd uint32
	si, x1a2        uint32
	x1a0            [9]uint32
}

func (usr *usrImpl) decryptInit() {
	var tmp byte
	usr.si = 0
	usr.x1a2 = 0

	for tmp = 0; tmp < 16; tmp++ {
		usr.decryptKey[tmp] = key[tmp]
	}
}

func (usr *usrImpl) EncryptBlock(str []byte) []byte {
	var n int
	var i byte
	usr.decryptInit()
	str = append([]byte{0x02, 0x03}, str...)
	size := len(str)
	str = reverse(str)
	for n = 0; n < size; n++ {
		usr.pc1assemble128()
		usr.cfc = usr.inter >> 8

		usr.cfd = usr.inter & 255
		for i = 0; i < 16; i++ {
			usr.decryptKey[i] = (usr.decryptKey[i] ^ (str[n])) & 0xFF
		}
		str[n] = byte(uint32(str[n])^(usr.cfc^usr.cfd)) & 0xff
	}
	return str
}

func (usr *usrImpl) DecryptBlock(str []byte) []byte {
	usr.decryptInit()
	size := len(str)
	for n := 0; n < size; n++ {
		usr.pc1assemble128()
		usr.cfc = usr.inter >> 8
		usr.cfd = usr.inter & 255

		str[n] = byte(uint32(str[n])^(usr.cfc^usr.cfd)) & 0xff
		for i := 0; i < 16; i++ {
			usr.decryptKey[i] = (usr.decryptKey[i] ^ (str[n])) & 0xff
		}
	}
	result := bytes.Split(str, []byte{0x02, 0x03})
	return result[0]
}

func reverse(s []byte) []byte {

	var c byte
	j := len(s) - 1
	for i := 0; i < j; i++ {
		c = s[i]
		s[i] = s[j]
		s[j] = c
		j--
	}
	return s
}

func exchange(a *uint32, b *uint32) {
	var tmp uint32
	tmp = *a
	*a = *b
	*b = tmp
}

func (usr *usrImpl) pc1assemble128() {
	var i uint32
	var ax, bx, cx, dx uint32

	// unsigned int ax, bx, cx, dx;
	usr.inter = 0
	usr.x1a0[0] = 0
	for i = 0; i < 8; i++ {
		// fmt.Println("$$$$$$$", x1a0[i], uint(decryptKey[i*2])*256, uint(decryptKey[i*2+1]))
		usr.x1a0[i+1] = usr.x1a0[i] ^ (uint32(usr.decryptKey[i*2])*256 + uint32(usr.decryptKey[i*2+1]))
		dx = usr.x1a2 + i
		ax = usr.x1a0[i+1]
		cx = 0x015A
		bx = 0x4E35

		exchange(&ax, &usr.si)
		exchange(&ax, &dx)
		// fmt.Println("F1", ax, dx, si, bx)
		if ax != 0 {
			ax = ax * bx
		}
		// fmt.Println("F2", ax, bx)
		exchange(&ax, &cx)
		// fmt.Println("F3", ax, cx)

		if ax != 0 {
			ax = ax * usr.si
			cx = ax + cx
		}
		// fmt.Println("F4", ax, si, cx)

		exchange(&ax, &usr.si)
		// fmt.Println("F5", ax, si)
		ax = ax * bx
		dx = cx + dx
		ax = ax + 1
		// fmt.Println("F6", inter, ax, dx)
		usr.x1a2 = dx
		usr.x1a0[i+1] = ax
		usr.inter = usr.inter ^ (ax ^ dx)
		// fmt.Println("inter", inter)
	}
}
