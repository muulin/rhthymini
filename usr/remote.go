package usr

import (
	"bytes"
	"errors"
	"fmt"
	"net"
	"strings"
	"time"

	"github.com/94peter/sterna/log"
)

const (
	CMD_QueryMAC       = "get_mac"
	CMD_QueryNetEnable = "get_net_enable"
	CMD_QueryNetMode   = "get_net_mode"
	CMD_Switch4GFirst  = "switch_4g_first"
	CMD_SwitchWanFirst = "switch_Wan_first"
	CMD_Reboot         = "reboot"
)

var CmdMap = map[string]string{
	CMD_QueryMAC:       `AT+MAC`,
	CMD_QueryNetEnable: `at+linuxcmp=uci get netswitch.@netswitch[0].netswitch_enable`,
	CMD_QueryNetMode:   `at+linuxcmp=uci get netswitch.@netswitch[0].wanprio`,
	CMD_Switch4GFirst:  `at+linuxcmp=uci set netswitch.@netswitch[0].wanprio='4g wan sta'`,
	CMD_SwitchWanFirst: `at+linuxcmp=uci set netswitch.@netswitch[0].wanprio='wan sta 4g'`,
	CMD_Reboot:         `at+linuxcmp=reboot`,
}

var cmdParserFromBack = func(content string) string {
	buf := &bytes.Buffer{}
	for i := len(content) - 2; ; i-- {
		if content[i] == '&' {
			break
		}
		buf.WriteByte(content[i])
	}

	s := buf.Bytes()
	for i, j := 0, len(s)-1; i < j; i, j = i+1, j-1 {
		s[i], s[j] = s[j], s[i]
	}
	return string(s)
}

var CmdParserMap = map[string]RemoteRespParser{
	"default": func(content string) string {
		return content
	},
	CMD_QueryMAC: func(content string) string {
		result := strings.Split(content, ":")
		if len(result) == 1 {
			return result[0]
		} else {
			return result[1][:len(result[1])-1]
		}
	},
	CMD_QueryNetEnable: cmdParserFromBack,
	CMD_QueryNetMode:   cmdParserFromBack,
	CMD_Switch4GFirst:  cmdParserFromBack,
	CMD_SwitchWanFirst: cmdParserFromBack,
}

func GetRemoteKey(id string) string {
	return "cmd_" + id
}

func GetRemoteRespKey(id string) string {
	return "cmd_res_" + id
}

type RemoteRespParser func(content string) string

type Remote interface {
	Remote(id, cmd string) (RemoteRespParser, error)
}

func NewRemote(c net.Conn, l log.Logger) Remote {
	return &remoteImpl{
		Conn:       c,
		Encryption: NewEncryption(),
		l:          l,
	}
}

type remoteImpl struct {
	net.Conn
	Encryption
	l log.Logger
}

func (tcp *remoteImpl) Remote(id, cmd string) (RemoteRespParser, error) {
	var exeCmd string
	var ok bool
	if exeCmd, ok = CmdMap[cmd]; !ok {
		return nil, errors.New("cmd not exist: " + cmd)

	}
	err := tcp.query(id, exeCmd)
	if err != nil {
		return nil, err
	}
	time.Sleep(time.Second * 1)
	if respParser, ok := CmdParserMap[cmd]; ok {
		return respParser, nil
	}
	return CmdParserMap["default"], nil
}

func (tcp *remoteImpl) query(id, content string) error {
	cmd := []byte(fmt.Sprintf(`{"msg":"remoteQuery","id":"%s","content":"%s"}`, id, content))
	tcp.l.Debug(string(cmd))
	_, err := tcp.Write(tcp.EncryptBlock(cmd))
	return err
}
