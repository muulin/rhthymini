package main

import (
	"context"
	"flag"
	"fmt"
	"net/http"
	"path/filepath"
	myapi "rhthymini/api"
	mymid "rhthymini/api/mid"
	"rhthymini/model"

	"github.com/94peter/sterna"
	"github.com/94peter/sterna/api"
	"github.com/94peter/sterna/api/mid"
	"github.com/94peter/sterna/auth"
	"github.com/94peter/sterna/db"
	"github.com/94peter/sterna/log"
	"github.com/94peter/sterna/model/mgom"
	"github.com/gorilla/mux"
	"github.com/rs/cors"
)

var (
	confpath = flag.String("confpath", "./", "write memory profile to `file`")
	v        = flag.Bool("v", false, "version")

	Version   = "1.0.0"
	BuildTime = "2000-01-01T00:00:00+0800"
)

func main() {
	flag.Parse()

	if *v {
		fmt.Println("Version: " + Version)
		fmt.Println("Build Time: " + BuildTime)
		return
	}

	di := &di{}
	filename, _ := filepath.Abs(*confpath + "config.yml")
	sterna.InitConfByFile(filename, di)

	if di.TcpServConf == nil {
		panic("missing tcpServ conf")
	}
	l := di.NewLogger("rhthymini")

	ctx := context.Background()
	redis, err := di.NewRedisClient(ctx)
	if err != nil {
		panic(err)
	}

	// user, uok := os.LookupEnv("MONGO_USER")
	// pwd, pok := os.LookupEnv("MONGO_PWD")
	// if uok && pok {
	// 	di.SetAuth(user, pwd)
	// }

	// mgoClt, err := di.NewMongoDBClient(ctx, "")
	// if err != nil {
	// 	panic(err)
	// }
	// mgo := mgom.NewMgoModel(ctx, mgoClt.GetCoreDB(), l)

	// start api service
	router := mux.NewRouter()
	routerAPI := myapi.NewRouterAPI(l, "router")
	di.InitAPI(
		router,
		[]mid.Middle{
			mid.NewDebugMid("debug"),
			mymid.NewDBMid(di, "db"),
		},
		nil,
		routerAPI,
	)

	chanConn := make(chan *model.TcpConn, 1)
	// start tcp service

	go func(conn chan *model.TcpConn, rc db.RedisClient, m mgom.MgoDBModel, l log.Logger) {
		err := di.TcpServConf.New(chanConn, redis, m, l).Start()
		if err != nil {
			panic(err)
		}
	}(chanConn, redis, nil, l)

	go func(api myapi.RounterAPI, chanConn chan *model.TcpConn, l log.Logger) {
		var tcpConn *model.TcpConn
		for {
			select {
			case tcpConn = <-chanConn:
				l.Debug("main receipt: " + tcpConn.ID)
				api.AddTcpConn(tcpConn)
			}
		}
	}(routerAPI, chanConn, l)

	l.Info(fmt.Sprintf("start api service with version %s and build time %s", Version, BuildTime))
	if di.EnableCORS() {
		c := cors.New(cors.Options{
			AllowedOrigins:   []string{"*"},
			AllowCredentials: true,
			AllowedHeaders:   []string{"Auth-Token", "Content-Type"},
			AllowedMethods:   []string{"POST", "GET", "PUT", "DELETE"},
			// Enable Debugging for testing, consider disabling in production
			// Debug: true,
		})
		l.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", di.GetPort()), c.Handler(router)).Error())
	} else {
		l.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", di.GetPort()), router).Error())
	}
}

type di struct {
	*model.TcpServConf `yaml:"tcpServ,omitempty"`
	*db.MongoConf      `yaml:"mongo,omitempty"`
	*log.LoggerConf    `yaml:"log,omitempty"`
	*api.APIConf       `yaml:"api,omitempty"`
	*auth.JwtConf      `yaml:"jwtConf"`
	*db.RedisConf      `yaml:"redis,omitempty"`
}
