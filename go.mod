module rhthymini

go 1.15

require (
	github.com/94peter/sterna v0.12.5
	github.com/google/uuid v1.3.0
	github.com/gorilla/mux v1.8.0
	github.com/kr/text v0.2.0 // indirect
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	github.com/rs/cors v1.8.2
	github.com/stretchr/testify v1.7.0
	go.mongodb.org/mongo-driver v1.8.2 // indirect
	golang.org/x/net v0.0.0-20220114011407-0dd24b26b47d // indirect
	golang.org/x/sys v0.0.0-20211007075335-d3039528d8ac // indirect
	gopkg.in/check.v1 v1.0.0-20200227125254-8fa46927fb4f // indirect
)
