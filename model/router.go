package model

import (
	"bytes"
	"encoding/gob"
	"rhthymini/usr"
	"time"

	"github.com/94peter/sterna/db"
)

type RouterCRUD interface {
	UpdateExpired(id string) error
	Exists(id string) bool
	Save(content *usr.AutoUploadContent) error
	FindByID(id string) (*usr.AutoUploadContent, error)
	SetResp(id, content string) error
	GetResp(id string) ([]byte, error)
}

func NewRouterCRUD(redis db.RedisClient) RouterCRUD {
	return &routerCRUDImpl{
		RedisClient: redis,
	}
}

type routerCRUDImpl struct {
	db.RedisClient
}

func (crud *routerCRUDImpl) UpdateExpired(id string) error {
	_, err := crud.Expired(id, time.Minute)
	return err
}

func (crud *routerCRUDImpl) Exists(id string) bool {
	return crud.RedisClient.Exists(id)
}

func (crud *routerCRUDImpl) FindByID(id string) (*usr.AutoUploadContent, error) {
	data, err := crud.Get(id)
	if err != nil {
		return nil, err
	}
	content := &usr.AutoUploadContent{}

	err = gob.NewDecoder(bytes.NewBuffer(data)).Decode(content)
	if err != nil {
		return nil, err
	}
	return content, nil
}

func (crud *routerCRUDImpl) Save(content *usr.AutoUploadContent) error {
	var data bytes.Buffer
	enc := gob.NewEncoder(&data)
	err := enc.Encode(content)
	if err != nil {
		return err
	}
	_, err = crud.Set(content.Mac, data.Bytes(), time.Minute)
	return err
}

func (crud *routerCRUDImpl) SetResp(id, content string) error {
	_, err := crud.Set(usr.GetRemoteRespKey(id), content, 0)
	return err
}
func (crud *routerCRUDImpl) GetResp(id string) ([]byte, error) {
	key := usr.GetRemoteRespKey(id)

	result, err := crud.Get(key)
	if err == nil {
		crud.Del(key)
		return result, nil
	}
	errTimes := 1
	for errTimes < 3 {
		time.Sleep(time.Second * 2)
		result, err = crud.Get(key)
		if err == nil {
			crud.Del(key)
			return result, nil
		}
		errTimes++
	}
	return nil, err
}
