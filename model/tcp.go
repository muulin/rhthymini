package model

import (
	"fmt"
	"net"
	"regexp"
	"rhthymini/usr"
	"sync"
	"time"

	"github.com/94peter/sterna/db"
	"github.com/94peter/sterna/log"
	"github.com/94peter/sterna/model/mgom"
)

type TcpService interface {
	Start() error
}

type TcpServConf struct {
	Port string `json:"port"`
}

const (
	TCP_ACT_ACCEPT = "accept"
	TCP_ACT_CLOSE  = "close"
)

type TcpConn struct {
	ID   string
	Conn net.Conn
	Act  string
}

func (conf *TcpServConf) New(conn chan *TcpConn, rc db.RedisClient, mgo mgom.MgoDBModel, l log.Logger) TcpService {
	crud := NewRouterCRUD(rc)
	return &tcpImpl{
		TcpServConf: conf,
		crud:        crud,
		mgo:         mgo,
		log:         l,
		chanConn:    conn,
	}
}

type tcpImpl struct {
	*TcpServConf
	chanConn chan *TcpConn
	crud     RouterCRUD
	mgo      mgom.MgoDBModel
	log      log.Logger
}

func (tcp *tcpImpl) Start() error {
	if err := tcp.run(); err != nil {
		tcp.log.Err(err.Error())
		return err
	}
	return nil
}

func (tcp *tcpImpl) run() error {
	tcp.log.Info(fmt.Sprintf("start listen tcp port [%s]", tcp.Port))
	l, err := net.Listen("tcp", ":"+tcp.Port)
	if err != nil {
		return err
	}
	defer l.Close()
	buffer := make([]byte, 14)
	usrEncryp := usr.NewEncryption()
	r, _ := regexp.Compile("^[0-9A-Fa-f]{12}$")
	for {
		c, err := l.Accept()
		if err != nil {
			return err
		}
		// get id
		tcp.log.Debug("receipt: " + time.Now().Format(time.RFC3339))
		n, err := c.Read(buffer)
		if err != nil {
			break
		}
		tcp.log.Debug(fmt.Sprintf("receipt data len: %d", n))
		data := buffer[:n]
		data = usrEncryp.DecryptBlock(data)
		if !r.Match(data) {
			tcp.log.Info("can not parser: " + string(data))
			c.Close()
			continue
		}
		id := string(data)

		tcp.log.Debug(id)
		hadnler := NewTcpHandler(tcp.chanConn, c, id, tcp.crud, tcp.mgo, tcp.log)
		tcp.log.Debug("chan pass: " + id)
		tcp.chanConn <- &TcpConn{
			ID:   id,
			Conn: c,
			Act:  TCP_ACT_ACCEPT,
		}
		go hadnler.handle()
	}
	return err
}

type TcpHandler interface {
	handle()
}

func NewTcpHandler(chanConn chan *TcpConn, c net.Conn, id string, crud RouterCRUD, mgo mgom.MgoDBModel, l log.Logger) TcpHandler {
	return &handlerImpl{
		chanConn: chanConn,
		id:       id,
		Conn:     c,
		buffer:   make([]byte, 1024),
		crud:     crud,
		mgo:      mgo,
		log:      l,
	}
}

type handlerImpl struct {
	id string
	net.Conn
	buffer []byte

	chanConn chan *TcpConn
	crud     RouterCRUD
	mgo      mgom.MgoDBModel
	log      log.Logger
}

func (tcp *handlerImpl) handle() {
	usrEncryp := usr.NewEncryption()
	var n int
	buffer := make([]byte, 1024)
	defer tcp.Close()
	var err error
	var mux sync.Mutex
	for {
		tcp.SetDeadline(time.Now().Add(time.Minute))
		n, err = tcp.Read(buffer)
		if err != nil {
			break
		}
		tcp.log.Debug(fmt.Sprintf("receipt data len: %d", n))
		cpData := make([]byte, n)
		copy(cpData, buffer[:n])
		go func(d []byte) {
			mux.Lock()
			defer mux.Unlock()
			d = usrEncryp.DecryptBlock(d)
			tcp.log.Debug(string(d))
			if usr.IsHeartpkt(d) {
				// 更新redis expired time
				if err = tcp.heartPkgHandler(); err != nil {
					tcp.log.Err("update expred time fail: " + err.Error())
				}

				return
			}
			parser, err := usr.NewParser(d)
			if err != nil {
				tcp.log.Warn("parser fail: " + err.Error())
				return
			}
			// 儲存Auto Upload Response
			if parser.IsAutoUploadRes() {
				if res := parser.AutoUploadRes(); res != nil {
					tcp.log.Debug(res.NetStatus)
					tcp.autoUploadHandler(res)
				}
			} else {
				err = tcp.crud.SetResp(parser.GetMAC(), parser.GetContent())
				if err != nil {
					tcp.log.Err("set resp fail: " + err.Error())
				}
			}

		}(cpData)
	}
	if err != nil {
		tcp.log.Warn("end with error: " + err.Error())
	}
	tcp.chanConn <- &TcpConn{
		ID:   tcp.id,
		Conn: tcp.Conn,
		Act:  TCP_ACT_CLOSE,
	}
	return
}

func (tcp *handlerImpl) heartPkgHandler() error {
	var err error
	if tcp.crud.Exists(tcp.id) {
		err = tcp.crud.UpdateExpired(tcp.id)
	} else {
		err = tcp.crud.Save(&usr.AutoUploadContent{
			Mac: tcp.id,
		})
	}
	return err
}

func (tcp *handlerImpl) autoUploadHandler(content *usr.AutoUploadContent) error {
	return tcp.crud.Save(content)
}
