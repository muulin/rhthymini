package api

import (
	"encoding/json"
	"io"
	"net"
	"net/http"
	"rhthymini/model"
	"rhthymini/usr"
	"strconv"
	"sync"

	"github.com/94peter/sterna/api"
	"github.com/94peter/sterna/db"
	"github.com/94peter/sterna/log"
	"github.com/94peter/sterna/util"
)

type RounterAPI interface {
	api.API
	AddTcpConn(conn *model.TcpConn)
}

type routerAPI struct {
	mu       sync.Mutex
	Name     string
	connPool map[string]net.Conn
	l        log.Logger
}

func (api *routerAPI) GetName() string {
	return api.Name
}

func NewRouterAPI(l log.Logger, name string) RounterAPI {
	return &routerAPI{
		connPool: make(map[string]net.Conn),
		Name:     name,
		l:        l,
	}
}

func (a *routerAPI) GetAPIs() []*api.APIHandler {
	return []*api.APIHandler{
		{Path: "/v1/router/state", Next: a.onlineStateEndpoint, Method: "GET", Auth: false},
		{Path: "/v1/router/{ID}", Next: a.detailEndpoint, Method: "GET", Auth: false},
		{Path: "/v1/router/{ID}/conn", Next: a.getConnEndpoint, Method: "GET", Auth: false},
		{Path: "/v1/router/{ID}/_remote", Next: a.remoteConnEndpoint, Method: "POST", Auth: false},
	}
}

func (a *routerAPI) Init() {

}

func (a *routerAPI) AddTcpConn(conn *model.TcpConn) {
	a.mu.Lock()
	defer a.mu.Unlock()
	switch conn.Act {
	case model.TCP_ACT_ACCEPT:
		a.connPool[conn.ID] = conn.Conn
	case model.TCP_ACT_CLOSE:
		delete(a.connPool, conn.ID)
	}
}

func (api *routerAPI) remoteConnEndpoint(w http.ResponseWriter, req *http.Request) {
	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"].(string)
	if conn, ok := api.connPool[queryID]; !ok {
		w.WriteHeader(http.StatusNotFound)
		return
	} else {
		defer req.Body.Close()
		l := log.GetCtxLog(req)
		b, err := io.ReadAll(req.Body)
		// b, err := ioutil.ReadAll(resp.Body)  Go.1.15 and earlier
		if err != nil {
			l.Fatal(err.Error())
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(err.Error()))
			return
		}
		r := usr.NewRemote(conn, l)
		parser, err := r.Remote(queryID, string(b))
		if err != nil {
			l.Err(err.Error())
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(err.Error()))
			return
		}

		crud := model.NewRouterCRUD(db.GetRedisByReq(req))
		resp, err := crud.GetResp(queryID)
		if err != nil {
			l.Err(err.Error())
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(err.Error()))
			return
		}
		w.Write([]byte(parser(string(resp))))
	}
}

func (api *routerAPI) getConnEndpoint(w http.ResponseWriter, req *http.Request) {
	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"].(string)
	if _, ok := api.connPool[queryID]; ok {
		w.Write([]byte("ok"))
		return
	}

}

func (api *routerAPI) onlineStateEndpoint(w http.ResponseWriter, req *http.Request) {
	// redis := db.GetRedisByReq(req)
	// if redis == nil {
	// 	w.WriteHeader(http.StatusServiceUnavailable)
	// 	w.Write([]byte("redis is nil"))
	// 	return
	// }
	// num, _ := redis.CountKeys()
	num := len(api.connPool)
	w.Write([]byte(strconv.Itoa(num)))
}

func (api *routerAPI) detailEndpoint(w http.ResponseWriter, req *http.Request) {
	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"].(string)
	redis := db.GetRedisByReq(req)
	crud := model.NewRouterCRUD(redis)
	content, err := crud.FindByID(queryID)
	if err != nil {
		w.WriteHeader(http.StatusNoContent)
		return
	}
	// redis.Set("detail", "", time.Minute)
	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(content)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}
