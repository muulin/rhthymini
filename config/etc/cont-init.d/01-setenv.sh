#!/usr/bin/with-contenv sh

case $environment in
  "rhthymini-prod" )
     mv /etc/config-prod /conf
    ;;
  "beta" )
     mv /etc/config-beta /conf
    ;;
esac
